import os
import numpy as np
import matplotlib.pyplot as plt
import cv2
import pandas as pd


# import argparse
# parser = argparse.ArgumentParser(description='PONG Catastrophe Processing')
# parser.add_argument('--log_root', default='runs/', help='directory to store logs')
# parser.add_argument('--eval', action='store_true', default=False, help='run only evaluation')
# args = parser.parse_args()


def get_image_class(img):
	if (img[84, 70:72] == 147).all():
		# images where the paddle touches or goes under the edge
		# print('too low')
		return -1
	elif (img[5, 70:72] == 147).all():
		# images where the paddle touches or goes under the edge
		# print('too high')
		return -1
	elif img[0, 0] == 236:
		# all other valid images
		# print('normal')
		return 1
	else:
		# invalid image colors
		# print('ERROR!!!')
		return -1


def get_catastrophe_class(img, action):
	if (img[81, 70:72] == 147).all() and (img[84, 70:72] == 87).all():
		# images where the paddle almost touches the bottom edge
		# print('almost touches bottom edge')
		if action in [3, 5]:
			# action "move down"
			return 'bottom'
	elif (img[5, 70:72] == 87).all() and (img[8, 70:72] == 147).all():
		# images where the paddle almost touches the bottom edge
		# print('almost touches bottom edge')
		if action in [2, 4]:
			# action "move down"
			return 'top'

	return 'none'


IMAGES_DIR = "/home/lukas/Workspaces/DTU/runs/dqn/pong/2019-17-12-12:49:14/states"
# NEW_IMAGES_DIR = "/home/lukas/Workspaces/DTU/runs/dqn/pong/2019-17-12-12:49:14/pixel-line-states-black-bg-cropped"
NEW_IMAGES_DIR = "/home/lukas/Workspaces/DTU/runs/dqn/pong/2019-17-12-12:49:14/no-line-states-black-bg"
CSV_FILE = "/home/lukas/Workspaces/DTU/runs/dqn/pong/2019-17-12-12:49:14/state-action-catastrophe.csv"
NEW_CSV_FILE = "/home/lukas/Workspaces/DTU/runs/dqn/pong/2019-17-12-12:49:14/state-action-catastrophe_labeled.csv"

sac_df = pd.read_csv(CSV_FILE, index_col=0)
new_df = []
for i, row in sac_df.iterrows():
	# determine image class
	img = cv2.imread(f"{IMAGES_DIR}/{row.img_name}", 0)
	if get_image_class(img) == 1:
		new_df.append({'img_name': row.img_name, 'action': row.action, 'catastrophe': get_catastrophe_class(img, row.action)})

	# set lower
	img[:4, :] = 87  # leaves only 1-pixel wide upper line
	img[86:, :] = 87  # leaves only 1-pixel wide bottom line
	img[img < 90] = 0
	# img[img >= 90] = 255
	# img[4, :] = 127
	# img[85, :] = 127
	img[4, :] = 0
	img[85, :] = 0
	# print(img.shape)
	img_2 = img[5:85, :]
	# print(img_2.shape)
	# print(img_2[:3, :])
	# print(img_2[-3:, :])

	# print(img[80:, :10])
	cv2.imwrite(f"{NEW_IMAGES_DIR}/{row.img_name}", img)
	# cv2.imwrite(f"{NEW_IMAGES_DIR}/{row.img_name}", img_2)
	# break


# print(pd.DataFrame(new_df)[pd.DataFrame(new_df).catastrophe == 'bottom'])
# labeled_df = pd.DataFrame(new_df)
# labeled_df.to_csv(NEW_CSV_FILE)

"""
# img = cv2.imread(f"{IMAGES_DIR}/0-17.png", 0)
# img = cv2.imread(f"{IMAGES_DIR}/0-403.png", 0)
# img = cv2.imread(f"{IMAGES_DIR}/0-410.png", 0)
# img = cv2.imread(f"{IMAGES_DIR}/0-403.png", 0)
img = cv2.imread(f"{IMAGES_DIR}/0-14.png", 0)

print(img.shape, type(img))
# print(img[70:, 65:75])
print(img[4:6, 70:72])
print(img[81:86, 70:72])
print(img[0,0])

get_image_class(img)

plt.figure()
plt.imshow(img,
           interpolation='none')
plt.title('Example extracted screen')
plt.show()
"""

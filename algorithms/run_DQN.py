import numpy as np
import random
import torch
import torch.optim as optim
import torch.nn.functional as F
import matplotlib.pyplot as plt
from environments.gridworld import GridWorld
from algorithms.DQN import DQN

import os
import cv2
import time
from collections import namedtuple
from itertools import count
import pandas as pd

N, M = 5, 5
env = GridWorld(n=N, m=M, water=True)

Transition = namedtuple('Transition', ('state', 'action', 'next_state', 'reward'))


class ReplayMemory:
    def __init__(self, capacity):
        self.capacity = capacity
        self.memory = []
        self.position = 0

    def push(self, state, action, new_state, reward):
        transition = Transition(state,
                                torch.tensor([[action]], dtype=torch.long),
                                new_state,
                                torch.tensor([reward], dtype=torch.float))
        if len(self.memory) < self.capacity:
            self.memory.append(None)
        self.memory[self.position] = transition
        self.position = (self.position + 1) % self.capacity

    def sample(self, batch_size):
        return random.sample(self.memory, batch_size)

    def __len__(self):
        return len(self.memory)


def get_screen():
    # Returned screen is NxM. Convert it into torch and add channel and batch dimensions.
    screen = env.render()
    # Convert to float, convert to torch tensor
    # (this doesn't require a copy)
    screen = np.ascontiguousarray(screen, dtype=np.float32)
    screen = torch.from_numpy(screen)
    # Add a batch and channel dimension (BCHW)
    if len(screen.shape) == 3:
        return screen.unsqueeze(0)
    elif len(screen.shape) == 2:
        return screen.unsqueeze(0).unsqueeze(0)
    else:
        raise NotImplementedError


def select_action(state, actions, eps):
    # max_action_index = np.argmax([q_table[state, a] for a in actions])
    # return actions[max_action_index]
    rand = np.random.random()
    if rand >= eps:
        with torch.no_grad():
            # t.max(1) will return largest column value of each row.
            # second column on max result is index of where max element was
            # found, so we pick action with the larger expected reward.
            action_values = policy_net(state)#.max(1)[1].view(1, 1).item()
            action = action_values.max(1)[1].view(1, 1).item()
            # print(action_values)
    else:
        action = env.sample_action_space()

    # print(action, actions)
    return actions[action]


def optimize_model():
    if len(memory) < BATCH_SIZE:
        return
    transitions = memory.sample(BATCH_SIZE)
    # Transpose the batch (see https://stackoverflow.com/a/19343/3343043 for
    # detailed explanation). This converts batch-array of Transitions
    # to Transition of batch-arrays.
    batch = Transition(*zip(*transitions))

    # Compute a mask of non-final states and concatenate the batch elements
    # (a final state would've been the one after which simulation ended)
    non_final_mask = torch.tensor(tuple(map(lambda s: s is not None, batch.next_state)),
                                  dtype=torch.bool)
    non_final_next_states = torch.cat([s for s in batch.next_state if s is not None])
    state_batch = torch.cat(batch.state)
    action_batch = torch.cat(batch.action)
    reward_batch = torch.cat(batch.reward)

    # Compute Q(s_t, a) - the model computes Q(s_t), then we select the
    # columns of actions taken. These are the actions which would've been taken
    # for each batch state according to policy_net
    state_action_values = policy_net(state_batch).gather(1, action_batch)

    # Compute V(s_{t+1}) for all next states.
    # Expected values of actions for non_final_next_states are computed based
    # on the "older" target_net; selecting their best reward with max(1)[0].
    # This is merged based on the mask, such that we'll have either the expected
    # state value or 0 in case the state was final.
    next_state_values = torch.zeros(BATCH_SIZE)
    next_state_values[non_final_mask] = target_net(non_final_next_states).max(1)[0].detach()
    # Compute the expected Q values
    expected_state_action_values = (next_state_values * GAMMA) + reward_batch

    # Compute Huber loss
    loss = F.smooth_l1_loss(state_action_values, expected_state_action_values.unsqueeze(1))

    # Optimize the model
    optimizer.zero_grad()
    loss.backward()
    for param in policy_net.parameters():
        param.grad.data.clamp_(-1, 1)
    optimizer.step()


def makedir(directory):
    try:
        os.makedirs(directory)
    except OSError:
        print(f"Creation of the directory {directory} failed")
    else:
        print(f"Results stored in directory {directory}")


start_time = time.strftime("%Y-%d-%m-%H:%M:%S", time.localtime(time.time()))

BATCH_SIZE = 32
MEMORY_SIZE = 10000
LEARNING_RATE = 1e-4
GAMMA = 0.9
EPISODES = 1500

# EPS_START = 0.9
EPS_START = 0.6
EPS_MIN = 0.005
# EPS_MIN = 0.5
eps = EPS_START
# EPS_DECAY = (eps - EPS_MIN) / EPISODES
EPS_DECAY = (eps - EPS_MIN) / 250
MAX_STEPS = 100
SHOW_EVERY = 100
GENERATE_DATASET = False
MOVING_AVERAGE_WINDOW = 50
TARGET_UPDATE = 50
OUT_FILE_DIR = f"runs/dqn/gridworld/static_water/{start_time}"
SAVE_RESULTS = True

env.reset()
init_screen = get_screen()
_, _, screen_height, screen_width = init_screen.shape
n_actions = len(env.possible_actions)

policy_net = DQN(in_features=N*M, n_actions=n_actions)
target_net = DQN(in_features=N*M, n_actions=n_actions)
target_net.load_state_dict(policy_net.state_dict())
target_net.eval()

optimizer = optim.RMSprop(policy_net.parameters(), lr=LEARNING_RATE)
memory = ReplayMemory(MEMORY_SIZE)

# q_table = {}
# for state in env.state_space:
#     for action in env.action_space:
#         q_table[state, action] = 0

########### Plot initial state ########################
# f, ax = plt.subplots(1, figsize=(5,5))
# ax.imshow(env.render().transpose((1, 2, 0)))
# ax.axis('off')
# f.savefig(f"runs/dqn/gridworld/static_water/init_state_{N}x{M}.png")
#######################################################

ep_rewards = []
ep_durations = []
aggr_ep_rewards = {'ep': [], 'avg': [], 'min': [], 'max': []}

if GENERATE_DATASET:
    makedir(f"{OUT_FILE_DIR}/states/")
    out_df = pd.DataFrame(columns=['img_name','action','catastrophe'])  # dataframe with image, action, and catastrophe label

for episode in range(EPISODES):
    if episode % 20 == 0:
        print('episode:', episode, '; eps:', eps)
    show = episode % SHOW_EVERY == 0  # True for every SHOW_EVERY'th episode

    # observation = env.reset()
    # state = observation
    state = env.reset(random_water=False)  # env.reset(random_water=True)  # each episode the water box is placed randomly in the gridworld
    # last_screen = get_screen()
    # current_screen = get_screen()
    # state = current_screen - last_screen
    # print(state)
    # print(state.shape)
    # break

    ep_reward = 0
    eps = max(EPS_MIN, eps - EPS_DECAY)

    t = 0
    for i in range(MAX_STEPS): #count():  # can be replaced by a for loop if we want to give up and move to next episode after some number of steps
        # print('step:', t)
        action = select_action(state, env.possible_actions, eps=eps)
        # eps = max(EPS_MIN, eps - EPS_DECAY)

        if GENERATE_DATASET:
            norm_img_arr = env.render()  # get current state
            scaling_factor = 1
            img = np.kron(norm_img_arr, np.ones((scaling_factor, scaling_factor)))  # this line upscales the image
            # img = norm_img_arr
            img = np.transpose(img, (1,2,0))
            cv2.imwrite(f"{OUT_FILE_DIR}/states/{episode}-{i}.png", img*255)

        new_observation, reward, done, _ = env.step(action)
        ep_reward += reward

        if GENERATE_DATASET:
            out_df = out_df.append({'img_name': f'{episode}-{i}.png', 'action': action, 'catastrophe': reward == -50}, ignore_index=True)

        # Observe new state
        # last_screen = current_screen
        # current_screen = get_screen()

        if not done:
            # new_state = current_screen - last_screen
            new_state = new_observation
            # max_future_action = select_action(q_table, new_observation, env.possible_actions)
            # max_future_q = q_table[new_observation, max_future_action]
            # # update Q
            # q_table[observation, action] = (1 - LEARNING_RATE) * q_table[observation, action] + LEARNING_RATE * (reward + DISCOUNT * max_future_q)
        else:
            new_state = None
            # q_table[observation, action] = 0

        # Store the transition in memory
        memory.push(state, action, new_state, reward)

        # Move to the next state
        state = new_state

        # display walk-through one episode
        if show:
            norm_img_arr = env.render()
            # print(norm_img_arr.transpose((1, 2, 0)).shape)
            cv2.imshow("image", norm_img_arr.transpose((1, 2, 0)))
            cv2.waitKey(200)

        # Perform one step of the optimization (on the target network)
        optimize_model()
        t += 1
        # if episode == 480:
        #     f, ax = plt.subplots(1, figsize=(5,5))
        #     ax.imshow(env.render().transpose((1, 2, 0)))
        #     ax.axis('off')
        #     f.savefig(f"runs/dqn/gridworld/static_water/walkthrough_episode/walkthrough_{N}x{M}_ep480_step{t}.png")
        if done:
            break

    ep_durations.append(t)

    # Update the target network, copying all weights and biases in DQN
    if episode % TARGET_UPDATE == 0:
        target_net.load_state_dict(policy_net.state_dict())

    # cv2.destroyAllWindows()  # destroys all created windows

    # print(f"Episode reward: {ep_reward}")
    ep_rewards.append(ep_reward)

    if show:
        avg_ep_reward = sum(ep_rewards[-SHOW_EVERY:]) / len(ep_rewards[-SHOW_EVERY:])
        min_ep_reward = min(ep_rewards[-SHOW_EVERY:])
        max_ep_reward = max(ep_rewards[-SHOW_EVERY:])

        aggr_ep_rewards['ep'].append(episode)
        aggr_ep_rewards['avg'].append(avg_ep_reward)
        aggr_ep_rewards['min'].append(min_ep_reward)
        aggr_ep_rewards['max'].append(max_ep_reward)
        print(f"ep {episode}; avg {avg_ep_reward}; min {min_ep_reward}; max {max_ep_reward}")
        print(env.grid)
        # env.render()
        # time.sleep(1)
    # print('Done!')
    # print('finished episode', episode)
    # time.sleep(10)

moving_avg = np.convolve(ep_rewards, np.ones((MOVING_AVERAGE_WINDOW,)) / MOVING_AVERAGE_WINDOW, mode='valid')
n_steps_moving_avg = np.convolve(ep_durations, np.ones((MOVING_AVERAGE_WINDOW,)) / MOVING_AVERAGE_WINDOW, mode='valid')

if SAVE_RESULTS:
    makedir(OUT_FILE_DIR)

if GENERATE_DATASET:
    out_df.to_csv(f"{OUT_FILE_DIR}/state-action-catastrophe.csv")

# plt.plot([i for i in range(len(moving_avg))], moving_avg)
# plt.ylabel(f"Reward {MOVING_AVERAGE_WINDOW}ma")
# plt.xlabel("episode #")
# plt.title("Moving average for episode rewards")
# if SAVE_RESULTS:
#     plt.savefig(f"{OUT_FILE_DIR}/ma_{N}x{M}_eps{EPS}_lr{LEARNING_RATE}_g{GAMMA}_batch{BATCH_SIZE}_mem{MEMORY_SIZE}.png")
# else:
#     plt.show()
#
# plt.plot([i for i in range(len(ep_durations))], ep_durations)
# plt.ylabel(f"# steps")
# plt.xlabel("episode #")
# plt.title("Number of steps per episode")
# if SAVE_RESULTS:
#     plt.savefig(
#         f"{OUT_FILE_DIR}/num_steps_{N}x{M}_eps{EPS}_lr{LEARNING_RATE}_g{GAMMA}_batch{BATCH_SIZE}_mem{MEMORY_SIZE}.png")
# else:
#     plt.show()
#
# plt.plot(aggr_ep_rewards['ep'], aggr_ep_rewards['avg'], label="avg rewards")
# plt.plot(aggr_ep_rewards['ep'], aggr_ep_rewards['min'], label="min rewards")
# plt.plot(aggr_ep_rewards['ep'], aggr_ep_rewards['max'], label="max rewards")
# plt.legend(loc=4)
# plt.ylabel(f"Episode reward")
# plt.xlabel("episode #")
# plt.title(f"Rewards for every {SHOW_EVERY} episode")
# if SAVE_RESULTS:
#     plt.savefig(
#         f"{OUT_FILE_DIR}/rewards_{N}x{M}_eps{EPS}_lr{LEARNING_RATE}_g{GAMMA}_batch{BATCH_SIZE}_mem{MEMORY_SIZE}.png")
# else:
#     plt.show()


def plot_episode_rewards(episode_rewards):
    f, ax = plt.subplots(1)
    ax.plot([i for i in range(len(episode_rewards))], episode_rewards)
    ax.set_ylabel(f"Total reward")
    ax.set_xlabel("Episode")
    ax.set_title("Cummulative reward per episode")
    if SAVE_RESULTS:
        f.savefig(
            f"{OUT_FILE_DIR}/cum_rewards_{N}x{M}_eps{EPS_START}-{EPS_MIN}_lr{LEARNING_RATE}_g{GAMMA}_batch{BATCH_SIZE}_mem{MEMORY_SIZE}.png")
    else:
        plt.show()


def plot_moving_average_reward(moving_avg):
    f, ax = plt.subplots(1)
    ax.plot([i for i in range(len(moving_avg))], moving_avg)
    ax.set_ylabel(f"Reward {MOVING_AVERAGE_WINDOW}ma")
    ax.set_xlabel("Episode")
    ax.set_title("Moving average of episode rewards")
    if SAVE_RESULTS:
        f.savefig(
            f"{OUT_FILE_DIR}/ma_{N}x{M}_eps{EPS_START}-{EPS_MIN}_lr{LEARNING_RATE}_g{GAMMA}_batch{BATCH_SIZE}_mem{MEMORY_SIZE}.png")
    else:
        plt.show()


def plot_num_steps_per_episode(ep_durations):
    f, ax = plt.subplots(1)
    ax.plot([i for i in range(len(ep_durations))], ep_durations)
    ax.set_ylabel(f"Number of steps")
    ax.set_xlabel("Episode")
    ax.set_title("Total number of steps per episode")
    if SAVE_RESULTS:
        f.savefig(
            f"{OUT_FILE_DIR}/num_steps_{N}x{M}_eps{EPS_START}-{EPS_MIN}_lr{LEARNING_RATE}_g{GAMMA}_batch{BATCH_SIZE}_mem{MEMORY_SIZE}.png")
    else:
        plt.show()


def plot_num_steps_moving_average(moving_avg):
    f, ax = plt.subplots(1)
    ax.plot([i for i in range(len(moving_avg))], moving_avg)
    ax.set_ylabel(f"Number of steps {MOVING_AVERAGE_WINDOW}ma")
    ax.set_xlabel("Episode")
    ax.set_title("Moving average of number of steps per episode")
    if SAVE_RESULTS:
        f.savefig(
            f"{OUT_FILE_DIR}/n_steps_ma_{N}x{M}_eps{EPS_START}-{EPS_MIN}_lr{LEARNING_RATE}_g{GAMMA}_batch{BATCH_SIZE}_mem{MEMORY_SIZE}.png")
    else:
        plt.show()


def plot_num_steps_with_rewards_moving_average(rewards_ma, steps_ma):
    f, ax = plt.subplots(1)
    ax.plot([i for i in range(len(rewards_ma))], rewards_ma, color='blue')
    ax.set_ylabel(f"Total reward {MOVING_AVERAGE_WINDOW}ma", color='blue')
    ax.set_xlabel("Episode")
    ax.set_title("Total rewards with number of steps per episode")

    ax2 = ax.twinx()
    ax2.plot([i for i in range(len(steps_ma))], steps_ma, color='gray')
    ax2.set_ylabel(f"Number of steps {MOVING_AVERAGE_WINDOW}ma", color='gray')

    f.tight_layout()

    if SAVE_RESULTS:
        f.savefig(
            f"{OUT_FILE_DIR}/n_steps_w_rewards_ma_{N}x{M}_eps{EPS_START}-{EPS_MIN}_lr{LEARNING_RATE}_g{GAMMA}_batch{BATCH_SIZE}_mem{MEMORY_SIZE}.png")
    else:
        plt.show()


def plot_rewards(aggr_ep_rewards):
    f, ax = plt.subplots(1)
    ax.plot(aggr_ep_rewards['ep'], aggr_ep_rewards['avg'], label="avg rewards")
    ax.plot(aggr_ep_rewards['ep'], aggr_ep_rewards['min'], label="min rewards")
    ax.plot(aggr_ep_rewards['ep'], aggr_ep_rewards['max'], label="max rewards")
    ax.legend(loc=4)
    ax.set_ylabel(f"Episode reward")
    ax.set_xlabel("Episode")
    ax.set_title(f"Rewards for every {SHOW_EVERY} episode")
    if SAVE_RESULTS:
        f.savefig(
            f"{OUT_FILE_DIR}/rewards_{N}x{M}_eps{EPS_START}-{EPS_MIN}_lr{LEARNING_RATE}_g{GAMMA}_batch{BATCH_SIZE}_mem{MEMORY_SIZE}.png")
    else:
        plt.show()


plot_episode_rewards(ep_rewards)
plot_moving_average_reward(moving_avg)
plot_num_steps_per_episode(ep_durations)
plot_num_steps_moving_average(n_steps_moving_avg)
plot_num_steps_with_rewards_moving_average(moving_avg, n_steps_moving_avg)
plot_rewards(aggr_ep_rewards)

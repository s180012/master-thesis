import os
import numpy as np
import argparse
import json
import logging
import time

import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from torch.distributions import Normal, Bernoulli

import torchvision
from torchvision import datasets, transforms

import seaborn as sns
import matplotlib.pyplot as plt

from torch.utils.tensorboard import SummaryWriter  # TensorBoard

import vae.model as vae_model
from vae.gridworld_dataset import GridworldDataset
from vae.pong_dataset import PongDataset

import sys

parser = argparse.ArgumentParser(description='VAE MNIST')
parser.add_argument('--log_root', default='runs/', help='directory to store logs')
parser.add_argument('--data_root', default='data/', help='directory to store the dataset')
parser.add_argument('--dataset', default='mnist', help='dataset')
parser.add_argument('--batch_size', type=int, default=64, help='input batch size (default: 64)')
parser.add_argument('--epochs', type=int, default=10, help='number of epochs (default: 100)')
parser.add_argument('--lr', type=float, default=1e-3, help='learning rate (default: 1e-3)')
parser.add_argument('--latent_dim', type=int, default=10, help='latent dimension (default: 10)')
parser.add_argument('--beta', type=float, default=1, help='beta for beta-VAE (default: 1) = basic VAE')
parser.add_argument('--seed', type=int, default=1, help='random seed (default: 1)')
parser.add_argument('--log_interval', type=int, default=100, help='logging frequency measured by number of training episodes')
parser.add_argument('--no-cuda', action='store_true', default=False, help='enables CUDA')
parser.add_argument('--log_elbo', action='store_true', default=False, help='stores elbo for each input into log file')
parser.add_argument('--eval_set', type=str, default='not_in_training', help='name of the evaluation set')
parser.add_argument('--eval', action='store_true', default=False, help='run only evaluation')
parser.add_argument('--comment', default='', help='additional comment to the run')

args = parser.parse_args()

# if gpu is to be used
args.cuda = not args.no_cuda and torch.cuda.is_available()
device = torch.device("cuda" if args.cuda else "cpu")

torch.manual_seed(args.seed)
np.random.seed(args.seed)

if not args.eval:
	start_time = time.strftime("%Y-%d-%m-%H:%M:%S", time.localtime(time.time()))
	if args.comment == '':
		# run_id = f"{args.dataset}-{args.latent_dim}-epochs{args.epochs}-beta{args.beta}-lr{args.lr}-batch{args.batch_size}-MLP-{start_time}'"
		run_id = f"{args.dataset}-{args.latent_dim}-epochs{args.epochs}-beta{args.beta}-lr{args.lr}-batch{args.batch_size}-{start_time}'"
	else:
		# run_id = f"{args.dataset}-{args.latent_dim}-epochs{args.epochs}-beta{args.beta}-lr{args.lr}-batch{args.batch_size}-MLP-{start_time}'_'{args.comment}"
		run_id = f"{args.dataset}-{args.latent_dim}-epochs{args.epochs}-beta{args.beta}-lr{args.lr}-batch{args.batch_size}-{start_time}'_'{args.comment}"
	log_dir = os.path.join(args.log_root, run_id)
	if not os.path.exists(log_dir):
		os.makedirs(log_dir)
	with open(os.path.join(log_dir, 'config.json'), 'w') as f:
		f.write(json.dumps(vars(args)))

# transform = transforms.Compose([transforms.ToTensor(),
# #                                 flatten
#                                ])


# create logger with 'spam_application'
logger = logging.getLogger(f'{args.dataset}_epochs{args.epochs}_beta{args.beta}_eval={args.eval}')
logger.setLevel(logging.DEBUG)
# create file handler which logs even debug messages
fh = logging.FileHandler(f'spam.log')
fh.setLevel(logging.DEBUG)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(fh)
logger.info('creating an instance of auxiliary_module.Auxiliary')



if args.dataset == 'mnist':
	train_set = datasets.MNIST(root=args.data_root, train=True, transform=transforms.ToTensor(), download=True)
	test_set = datasets.MNIST(root=args.data_root, train=False, transform=transforms.ToTensor(), download=True)
	img_size = (28, 28)
	in_channels = 1
elif args.dataset == 'fashionmnist':
	train_set = datasets.FashionMNIST(root=args.data_root+args.dataset, train=True, transform=transforms.ToTensor(), download=True)
	test_set = datasets.FashionMNIST(root=args.data_root+args.dataset, train=False, transform=transforms.ToTensor(), download=True)
	img_size = (28, 28)
	in_channels = 1
elif args.dataset == 'gridworld':
	# data_dir = f"{args.data_root}Gridworlds/2019-10-12-22:25:11"
	# data_dir = f"{args.data_root}Gridworlds/841-images"
	data_dir = f"{args.data_root}Gridworlds/moving_water/7x7"
	if args.eval:
		train_csv_file = f"{data_dir}/{args.eval_set}.csv"
		test_csv_file = f"{data_dir}/{args.eval_set}.csv"
		# train_csv_file = f"{data_dir}/test_states.csv"
		# test_csv_file = f"{data_dir}/test_states.csv"
		train_images_dir = f"{data_dir}/testset/"
		test_images_dir = f"{data_dir}/testset/"
	else:
		# train_csv_file = f"{data_dir}/state-action-catastrophe.csv"
		# test_csv_file = f"{data_dir}/test_states.csv"
		# train_images_dir = f"{data_dir}/states/"
		# test_images_dir = f"{data_dir}/testset/"
		train_csv_file = f"{data_dir}/train_set.csv"
		test_csv_file = f"{data_dir}/dev_set.csv"
		train_images_dir = f"{data_dir}/states/"
		test_images_dir = f"{data_dir}/states/"
	gridworld_train_set = GridworldDataset(csv_file=train_csv_file, images_dir=train_images_dir, transform=transforms.ToTensor())
	gridworld_test_set = GridworldDataset(csv_file=test_csv_file, images_dir=test_images_dir, transform=transforms.ToTensor())
	img_size = (7, 7)
	in_channels = 3
elif args.dataset == 'pong':
	print(args.data_root)
	train_csv_file = f"{args.data_root}/state-action-catastrophe_labeled.csv"
	test_csv_file = f"{args.data_root}/testset.csv"
	# images_dir = f"{args.data_root}/states/"
	train_images_dir = f"{args.data_root}/no-line-states-black-bg/"
	test_images_dir = f"{args.data_root}/test-set/"
	# images_dir = f"{args.data_root}/pixel-line-states-black-bg/"
	# images_dir = f"{args.data_root}/pixel-line-states-black-bg-cropped/"
	pong_train_set = PongDataset(csv_file=train_csv_file, images_dir=train_images_dir, transform=transforms.ToTensor(), classes=['none'])
	pong_test_set = PongDataset(csv_file=test_csv_file, images_dir=test_images_dir, transform=transforms.ToTensor(), classes=['none'])
	img_size = (80, 90)
	in_channels = 1
else:
	raise NotImplementedError

if args.dataset == 'gridworld':
	train_loader = torch.utils.data.DataLoader(dataset=gridworld_train_set, batch_size=args.batch_size, pin_memory=args.cuda, shuffle=True)
	test_loader = torch.utils.data.DataLoader(dataset=gridworld_test_set, batch_size=args.batch_size, pin_memory=args.cuda, shuffle=True)
elif args.dataset == 'pong':
	train_loader = torch.utils.data.DataLoader(dataset=pong_train_set, batch_size=args.batch_size, pin_memory=args.cuda, shuffle=True)
	test_loader = torch.utils.data.DataLoader(dataset=pong_test_set, batch_size=args.batch_size, pin_memory=args.cuda, shuffle=True)
	# print(pong_dataset.pong_df.head())
	# print(pong_dataset.pong_df.shape)
	# x, _ = next(iter(test_loader))
	# in_grid = torchvision.utils.make_grid(x.view(-1, in_channels, img_size[1], img_size[0]).cpu())
	# npimg = in_grid.numpy()
	# print(x.shape)
	# print(x.view(-1, in_channels, img_size[1], img_size[0]).shape)
	# print(in_grid.shape)
	# plt.imshow(np.transpose(npimg, (1,2,0)), interpolation='nearest')
	# plt.show()
	# print('sleeping')
	# time.sleep(100)
	# print('not sleeping')
else:
	from torch.utils.data.sampler import SubsetRandomSampler
	subset_indices = range(1000)
	train_loader = torch.utils.data.DataLoader(dataset=train_set, batch_size=args.batch_size, pin_memory=args.cuda, sampler=SubsetRandomSampler(subset_indices))
	test_loader = torch.utils.data.DataLoader(dataset=test_set, batch_size=args.batch_size, pin_memory=args.cuda, sampler=SubsetRandomSampler(subset_indices))
	# train_loader = torch.utils.data.DataLoader(dataset=train_set, batch_size=args.batch_size, pin_memory=args.cuda, shuffle=True)
	# test_loader = torch.utils.data.DataLoader(dataset=test_set, batch_size=args.batch_size, pin_memory=args.cuda, shuffle=True)

# encoder = vae_model.MLPEncoderOne(args.latent_dim, img_size).to(device)
# decoder = vae_model.MLPDecoderOne(args.latent_dim, img_size).to(device)
# encoder = vae_model.MLPEncoder(args.latent_dim, img_size).to(device)
# decoder = vae_model.MLPDecoder(args.latent_dim, img_size).to(device)
encoder = vae_model.EncoderPong(args.latent_dim).to(device)
decoder = vae_model.DecoderPong(args.latent_dim).to(device)
# encoder = vae_model.Encoder(args.latent_dim).to(device)
# decoder = vae_model.Decoder(args.latent_dim).to(device)
vae = vae_model.VAE(encoder, decoder).to(device)

if not args.eval:
	with open(os.path.join(log_dir, 'config.json'), 'a') as f:
		f.write("\nModel: \n%r" % vae)


# TODO: create separate file with loss functions
def ELBO_loss(x, z, q_zx, p_xz, p_z, beta, **kwargs):
	x = x.view(x.shape[0], -1)
	# print(x.shape)
	log_prob_p_xz = p_xz.log_prob(x).sum(dim=1)
	log_prob_p_z = p_z.log_prob(z).sum(dim=1)
	log_prob_q_zx = q_zx.log_prob(z).sum(dim=1)
	elbo_batch = log_prob_p_xz + beta*(log_prob_p_z - log_prob_q_zx)
	loss = -torch.mean(elbo_batch)
	kl = torch.mean(beta*(log_prob_p_z - log_prob_q_zx))
#     print(loss, torch.mean(log_prob_p_xz).item(), torch.mean(log_prob_p_z).item(), torch.mean(log_prob_q_zx).item())
	return loss, kl, elbo_batch


loss_function = ELBO_loss
optimizer = optim.Adam(vae.parameters(), lr=args.lr)


def train(epoch):
	vae.train()
	batch_loss = []

	train_loss = 0.0
	running_kl = 0.0
	for batch_idx, (x, _) in enumerate(train_loader):
		x = x.to(device)
		out = vae(x)
		loss, kl, _ = loss_function(x=x, beta=args.beta, **out)

		optimizer.zero_grad()
		loss.backward()
#         print(vae.decoder.fc1.weight.grad.sum())
		optimizer.step()

		batch_loss.append(loss.item())
		train_loss += loss.item()
		running_kl += kl.item()

		# if batch_idx == 0:
			# grid = torchvision.utils.make_grid(x)
			# train_writer.add_image('images', grid)
			# train_writer.add_graph(vae, x)

		if batch_idx % args.log_interval == args.log_interval - 1:
			print('[%d, %5d] loss: %.3f; kl: %.3f' % (epoch, batch_idx + 1, train_loss / args.log_interval, running_kl / args.log_interval))
			train_loss = 0.0
			running_kl = 0.0
			# print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
			# 	epoch, batch_idx * len(x), len(train_loader.dataset),
			# 	100. * batch_idx / len(train_loader),
			# 	loss.item() / len(x)))

	# print('====> Epoch: {} Average loss: {:.4f}'.format(epoch, train_loss / len(train_loader.dataset)))

	return np.mean(batch_loss)


def test(epoch, test_loader):
	vae.eval()
	batch_loss = []
	epoch_elbos = []

	test_loss = 0.0
	with torch.no_grad():
		for i, (x, _) in enumerate(test_loader):
			x = x.to(device)
			out = vae(x)
			loss, kl, elbo_batch = loss_function(x=x, beta=args.beta, **out)
			batch_loss.append(loss.item())
			test_loss += loss.item()
			epoch_elbos.extend(elbo_batch.cpu().detach().numpy().flatten())  # store the elbo for each input datapoint into a list
			if i == 0 and not args.eval:
				# Record input images
				in_grid = torchvision.utils.make_grid(x.view(-1, in_channels, img_size[1], img_size[0]).cpu())
				valid_writer.add_image('input images', in_grid)

				# Record reconstructed images
				out_grid = torchvision.utils.make_grid(out['z_mu'].view(-1, in_channels, img_size[1], img_size[0]).cpu())
				valid_writer.add_image('reconstructions', out_grid)

				break  # remove break to test on the whole 'dev' set instead of just one batch

	# test_loss /= len(test_loader.dataset)
	# print('====> Test set loss: {:.4f}'.format(test_loss))

	if args.log_elbo:
		epoch_elbos

	return np.mean(batch_loss), epoch_elbos


def save_model(model, log_dir):
    pth = os.path.join(log_dir, f"{args.dataset}_b{args.beta}_z{args.latent_dim}_ep{args.epochs}_model.pth")
    torch.save(model.state_dict(), pth)



def load_model(model, log_dir):
    model.load_state_dict(torch.load(os.path.join(log_dir, f"{args.dataset}_b{args.beta}_z{args.latent_dim}_ep{args.epochs}_model.pth")))


# if __name__ == '__main__':
if args.eval:
	# gridworld-10-epochs10-beta0-seed1-MLP-2019-15-12-12:06:35
	# gridworld-10-epochs10-beta1-seed1-MLP-2019-14-12-15:41:49
	# gridworld-10-epochs100-beta0-seed1-MLP-2019-15-12-12:17:44
	# gridworld-10-epochs100-beta1-seed1-MLP-2019-15-12-12:19:31
	# gridworld-10-epochs10-beta0-seed1-MLP-color-2019-15-12-17:15:50
	# gridworld-10-epochs10-beta1-seed1-MLP-color-2019-15-12-16:27:20
	# load_model(vae, '/home/lukas/Workspaces/DTU/deep-rl-thesis/algorithms/runs/gridworld-10-epochs10-beta1-seed1-MLP-color-2019-15-12-16:27:20')
	load_model(vae, '/home/lukas/Workspaces/DTU/deep-rl-thesis/algorithms/trained_models/gridworld/beta_0/latent_dim_10')
	vae.eval()
	_, epoch_elbos = test(0, test_loader)
	# print(epoch_elbos)
	eval_logger = logging.getLogger(f'{args.dataset}_epochs{args.epochs}_beta{args.beta}_test - {args.eval_set}')
	eval_logger.setLevel(logging.DEBUG)
	# create file handler which logs even debug messages
	fh = logging.FileHandler(f'eval_epoch_elbos.log')
	fh.setLevel(logging.DEBUG)
	# create formatter and add it to the handlers
	formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
	fh.setFormatter(formatter)
	# add the handlers to the logger
	eval_logger.addHandler(fh)
	eval_logger.info(epoch_elbos)
else:
	train_writer = SummaryWriter(os.path.join(log_dir, 'train'))
	valid_writer = SummaryWriter(os.path.join(log_dir, 'valid'))

	elbos_per_epoch = []
	# min_valid_loss = np.iinfo(np.int32).max  # set min validation loss to infinity
	# best_epoch = 0
	for epoch in range(1, args.epochs + 1):
		train_loss = train(epoch)
		save_model(vae, log_dir)
		valid_loss, epoch_elbos = test(epoch, test_loader)
		# if valid_loss < min_valid_loss:
		# 	min_valid_loss = valid_loss
		# 	best_epoch = epoch
		# 	save_model(vae, log_dir)
		elbos_per_epoch.append(epoch_elbos)

		# Record epoch loss and accuracy to TensorBoard
		train_writer.add_scalar('Loss', train_loss, epoch)
		valid_writer.add_scalar('Loss', valid_loss, epoch)

		# Record latent space interpolations
		n = 15
		latent_space = torch.zeros((n*args.latent_dim, args.latent_dim))
		for i in range(args.latent_dim):
			latent_space[i*n:(i+1)*n, i] = torch.from_numpy(np.linspace(-3, 3, n))
		# print(latent_space)
		recon_out = vae.decoder(latent_space.to(device))
		grid = torchvision.utils.make_grid(recon_out.view(-1, in_channels, img_size[1], img_size[0]).cpu(), nrow=n)
		valid_writer.add_image('latent space interpolation', grid)

		# Record params to TensorBoard
#		for name, param in vae.named_parameters():
#			train_writer.add_histogram(name, param, epoch)
#			train_writer.add_histogram(f'{name}.grad', param.grad, epoch)

		# TODO: log input, reconstruction, latent space images
		# TODO: check the following piece of code
		# with torch.no_grad():
		#     sample = torch.randn(64, 20).to(device)
		#     sample = model.decode(sample).cpu()
		#     save_image(sample.view(64, 1, 28, 28),
		#                'results/sample_' + str(epoch) + '.png')
	train_writer.close()
	valid_writer.close()
	# with open(os.path.join(log_dir, 'config.json'), 'a') as f:
	# 	f.write(f'best_epoch: {best_epoch}')


# TODO: code based on this post: https://towardsdatascience.com/build-a-fashion-mnist-cnn-pytorch-style-efb297e22582

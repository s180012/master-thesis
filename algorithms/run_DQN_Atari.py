import numpy as np
import random
import torch
import torch.optim as optim
import torch.nn.functional as F
import torchvision.transforms as T
import matplotlib.pyplot as plt
from PIL import Image
import gym
from algorithms.DQN import DQN, DQNAtari

import os
import cv2
import time
from collections import namedtuple
from itertools import count
import pandas as pd

# N, M = 5, 5
# env = GridWorld(n=N, m=M, water=True)
env = gym.make('Pong-v0')

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

Transition = namedtuple('Transition', ('state', 'action', 'next_state', 'reward'))


class ReplayMemory:
    def __init__(self, capacity):
        self.capacity = capacity
        self.memory = []
        self.position = 0

    def push(self, state, action, new_state, reward):
        transition = Transition(state,
                                torch.tensor([[action]], dtype=torch.long),
                                new_state,
                                torch.tensor([reward], dtype=torch.float))
        if len(self.memory) < self.capacity:
            self.memory.append(None)
        self.memory[self.position] = transition
        self.position = (self.position + 1) % self.capacity

    def sample(self, batch_size):
        return random.sample(self.memory, batch_size)

    def __len__(self):
        return len(self.memory)


transform = T.Compose([T.ToPILImage(),
                    # T.Resize(40, interpolation=Image.CUBIC),
                    T.ToTensor()])


def get_screen():
    # Returned screen is NxM. Convert it into torch and add channel and batch dimensions.
    # screen = env.render(mode='rgb_array').transpose((2, 0, 1))  # transpose to (CHW)
    # print(screen.shape)
    screen = cv2.cvtColor(env.render(mode='rgb_array'), cv2.COLOR_RGB2GRAY)
    screen = np.expand_dims(screen, axis=0)
    # print(screen.shape)

    _, screen_height, screen_width = screen.shape
    screen = screen[:, 25:205, :]
    screen = screen[:, ::2, ::2]  # Take every second pixel
    # Convert to float, rescale, convert to torch tensor
    # (this doesn't require a copy)
    screen = np.ascontiguousarray(screen, dtype=np.float32) / 255
    screen = torch.from_numpy(screen)
    # Resize, and add a batch dimension (BCHW)
    return transform(screen).unsqueeze(0).to(device)


def select_action(state, eps):
    rand = np.random.random()
    if rand >= eps:
        with torch.no_grad():
            # t.max(1) will return largest column value of each row.
            # second column on max result is index of where max element was
            # found, so we pick action with the larger expected reward.
            action_values = policy_net(state)#.max(1)[1].view(1, 1).item()
            return action_values.max(1)[1].view(1, 1)
    else:
        return torch.tensor([[random.randrange(n_actions)]], device=device, dtype=torch.long)


"""
env.reset()
plt.figure()
screen = get_screen().cpu().squeeze(0).permute(1, 2, 0).numpy()
# print(screen.dtype, screen.shape)
plt.imshow(screen,
           interpolation='none')
plt.title('Example extracted screen')
plt.show()
"""


def optimize_model():
    if len(memory) < BATCH_SIZE:
        return
    transitions = memory.sample(BATCH_SIZE)
    # Transpose the batch (see https://stackoverflow.com/a/19343/3343043 for
    # detailed explanation). This converts batch-array of Transitions
    # to Transition of batch-arrays.
    batch = Transition(*zip(*transitions))

    # Compute a mask of non-final states and concatenate the batch elements
    # (a final state would've been the one after which simulation ended)
    non_final_mask = torch.tensor(tuple(map(lambda s: s is not None, batch.next_state)),
                                  device=device, dtype=torch.bool)
    non_final_next_states = torch.cat([s for s in batch.next_state if s is not None])
    state_batch = torch.cat(batch.state)
    action_batch = torch.cat(batch.action)
    reward_batch = torch.cat(batch.reward)

    # Compute Q(s_t, a) - the model computes Q(s_t), then we select the
    # columns of actions taken. These are the actions which would've been taken
    # for each batch state according to policy_net
    state_action_values = policy_net(state_batch).gather(1, action_batch)

    # Compute V(s_{t+1}) for all next states.
    # Expected values of actions for non_final_next_states are computed based
    # on the "older" target_net; selecting their best reward with max(1)[0].
    # This is merged based on the mask, such that we'll have either the expected
    # state value or 0 in case the state was final.
    next_state_values = torch.zeros(BATCH_SIZE, device=device)
    next_state_values[non_final_mask] = target_net(non_final_next_states).max(1)[0].detach()
    # Compute the expected Q values
    expected_state_action_values = (next_state_values * GAMMA) + reward_batch

    # Compute Huber loss
    loss = F.smooth_l1_loss(state_action_values, expected_state_action_values.unsqueeze(1))

    # Optimize the model
    optimizer.zero_grad()
    loss.backward()
    for param in policy_net.parameters():
        param.grad.data.clamp_(-1, 1)
    optimizer.step()


def makedir(directory):
    try:
        os.makedirs(directory)
    except OSError:
        print(f"Creation of the directory {directory} failed")
    else:
        print(f"Results stored in directory {directory}")


start_time = time.strftime("%Y-%d-%m-%H:%M:%S", time.localtime(time.time()))

BATCH_SIZE = 32
MEMORY_SIZE = 10000
LEARNING_RATE = 1e-4
GAMMA = 0.9
EPISODES = 1#0000
eps = 1.0
EPS_MIN = 0.1
EPS_DECAY = (eps - EPS_MIN) / 1000000
MAX_STEPS = 10000
SHOW_EVERY = 100
GENERATE_DATASET = False
MOVING_AVERAGE_WINDOW = 50
TARGET_UPDATE = 50
# OUT_FILE_DIR = f"../runs/dqn/gridworld/static_water/{start_time}"
OUT_FILE_DIR = f"../runs/dqn/pong/{start_time}"
SAVE_RESULTS = False

env.reset()
init_screen = get_screen()
_, _, screen_height, screen_width = init_screen.shape
n_actions = env.action_space.n
print('n_actions:', n_actions)
print(env.unwrapped.get_action_meanings())

policy_net = DQNAtari(screen_height, screen_width, n_actions).to(device)
target_net = DQNAtari(screen_height, screen_width, n_actions).to(device)
target_net.load_state_dict(policy_net.state_dict())
target_net.eval()

optimizer = optim.RMSprop(policy_net.parameters(), lr=LEARNING_RATE)
memory = ReplayMemory(MEMORY_SIZE)

# q_table = {}
# for state in env.state_space:
#     for action in env.action_space:
#         q_table[state, action] = 0


if GENERATE_DATASET:
    makedir(f"{OUT_FILE_DIR}/states/")
    out_df = pd.DataFrame(columns=['img_name','action','catastrophe'])  # dataframe with image, action, and catastrophe label

render_step_min = 1000
render_step_max = 1200
game_scores = []
n_lost = 0
n_won = 0
cum_reward = 0
for step in range(MAX_STEPS):
    # print('step:', step)
    show = step % SHOW_EVERY == 0  # True for every SHOW_EVERY'th step

    # observation = env.reset()
    # state = observation
    # state = env.reset(random_water=False)  # env.reset(random_water=True)  # each episode the water box is placed randomly in the gridworld
    # Select and perform an action
    last_screen = get_screen()
    current_screen = get_screen()
    state = current_screen - last_screen
    # print(state)
    # print(state.shape)
    # break

    # print('step:', t)
    action = select_action(state, eps=eps)
    # action = torch.tensor([[5]], device=device, dtype=torch.long)
    eps = max(EPS_MIN, eps - EPS_DECAY)

    if GENERATE_DATASET:
        plt.imsave(f"{OUT_FILE_DIR}/states/{step}-{i}.png", get_screen().cpu().squeeze(0).permute(1, 2, 0).numpy())
        # cv2.imwrite(f"{OUT_FILE_DIR}/states/{step}-{i}.png", img*255)

    _, reward, done, _ = env.step(action.item())
    reward = torch.tensor([reward], device=device)
    cum_reward += reward.item()

    # Observe new state
    last_screen = current_screen
    current_screen = get_screen()

    if GENERATE_DATASET:
        out_df = out_df.append({'img_name': f'{step}-{i}.png', 'action': action.item(), 'catastrophe': 'tbd'}, ignore_index=True)

    if not done:
        next_state = current_screen - last_screen
    else:
        next_state = None

    # Store the transition in memory
    memory.push(state, action, next_state, reward)

    # Move to the next state
    state = next_state

    # if i % 5:
    # env.render()
        # plt.figure()
        # plt.imshow(get_screen().cpu().squeeze(0).permute(1, 2, 0).numpy(),
        #            interpolation='none')
        # plt.title('Example extracted screen')
        # plt.show()
    # time.sleep(0.005)
    # display walk-through one step
    # if show:
    #     print(f"step {t}; cum reward: {cum_reward}")
    #     norm_img_arr = env.render()
    #     cv2.imshow("image", norm_img_arr)
    #     cv2.waitKey(500)

    # Perform one step of the optimization (on the target network)
    optimize_model()

    # Update the target network, copying all weights and biases in DQN
    if step % TARGET_UPDATE == 0:
        target_net.load_state_dict(policy_net.state_dict())

    # cv2.destroyAllWindows()  # destroys all created windows

    if reward.item() == 1:
        # print('Agent just won a game!!!! reward=', reward.item())
        # game_scores.append(1)
        n_won += 1
    elif reward.item() == -1:
        # print('Agent just lost!!!! reward=', reward.item())
        # game_scores.append(-1)
        n_lost += 1
    if show:
        print(f"step {step}; action: {action.item()}; cum reward: {cum_reward}; since last time: {n_won} wins, {n_lost} losses")
        n_won = 0
        n_lost = 0
        # print(env.grid)
        # env.render()
        # time.sleep(0.1)
    
    # if step > render_step_min and step < render_step_max:
    #     print('step ', step, '; action ', action.item())
    #     env.render()
    # elif step == render_step_max:
    #     render_step_min += 2000
    #     render_step_max += 2000


print('Complete')
# env.render()
env.close()
# plt.ioff()
# plt.show()


# moving_avg = np.convolve(ep_rewards, np.ones((MOVING_AVERAGE_WINDOW,)) / MOVING_AVERAGE_WINDOW, mode='valid')

if SAVE_RESULTS:
    makedir(OUT_FILE_DIR)

if GENERATE_DATASET:
    out_df.to_csv(f"{OUT_FILE_DIR}/state-action-catastrophe.csv")

"""
# plt.plot([i for i in range(len(moving_avg))], moving_avg)
# plt.ylabel(f"Reward {MOVING_AVERAGE_WINDOW}ma")
# plt.xlabel("episode #")
# plt.title("Moving average for episode rewards")
# if SAVE_RESULTS:
#     plt.savefig(f"{OUT_FILE_DIR}/ma_{N}x{M}_eps{EPS}_lr{LEARNING_RATE}_g{GAMMA}_batch{BATCH_SIZE}_mem{MEMORY_SIZE}.png")
# else:
#     plt.show()
#
# plt.plot([i for i in range(len(ep_durations))], ep_durations)
# plt.ylabel(f"# steps")
# plt.xlabel("episode #")
# plt.title("Number of steps per episode")
# if SAVE_RESULTS:
#     plt.savefig(
#         f"{OUT_FILE_DIR}/num_steps_{N}x{M}_eps{EPS}_lr{LEARNING_RATE}_g{GAMMA}_batch{BATCH_SIZE}_mem{MEMORY_SIZE}.png")
# else:
#     plt.show()
#
# plt.plot(aggr_ep_rewards['ep'], aggr_ep_rewards['avg'], label="avg rewards")
# plt.plot(aggr_ep_rewards['ep'], aggr_ep_rewards['min'], label="min rewards")
# plt.plot(aggr_ep_rewards['ep'], aggr_ep_rewards['max'], label="max rewards")
# plt.legend(loc=4)
# plt.ylabel(f"Episode reward")
# plt.xlabel("episode #")
# plt.title(f"Rewards for every {SHOW_EVERY} episode")
# if SAVE_RESULTS:
#     plt.savefig(
#         f"{OUT_FILE_DIR}/rewards_{N}x{M}_eps{EPS}_lr{LEARNING_RATE}_g{GAMMA}_batch{BATCH_SIZE}_mem{MEMORY_SIZE}.png")
# else:
#     plt.show()


def plot_moving_average_reward(moving_avg):
    f, ax = plt.subplots(1)
    ax.plot([i for i in range(len(moving_avg))], moving_avg)
    ax.set_ylabel(f"Reward {MOVING_AVERAGE_WINDOW}ma")
    ax.set_xlabel("episode #")
    ax.set_title("Moving average for episode rewards")
    if SAVE_RESULTS:
        f.savefig(
            f"{OUT_FILE_DIR}/ma_{N}x{M}_eps{EPS_DECAY}_lr{LEARNING_RATE}_g{GAMMA}_batch{BATCH_SIZE}_mem{MEMORY_SIZE}.png")
    else:
        plt.show()


def plot_num_steps_per_episode(ep_durations):
    f, ax = plt.subplots(1)
    ax.plot([i for i in range(len(ep_durations))], ep_durations)
    ax.set_ylabel(f"# steps")
    ax.set_xlabel("episode #")
    ax.set_title("Number of steps per episode")
    if SAVE_RESULTS:
        f.savefig(
            f"{OUT_FILE_DIR}/num_steps_{N}x{M}_eps{EPS_DECAY}_lr{LEARNING_RATE}_g{GAMMA}_batch{BATCH_SIZE}_mem{MEMORY_SIZE}.png")
    else:
        plt.show()


def plot_rewards(aggr_ep_rewards):
    f, ax = plt.subplots(1)
    ax.plot(aggr_ep_rewards['ep'], aggr_ep_rewards['avg'], label="avg rewards")
    ax.plot(aggr_ep_rewards['ep'], aggr_ep_rewards['min'], label="min rewards")
    ax.plot(aggr_ep_rewards['ep'], aggr_ep_rewards['max'], label="max rewards")
    ax.legend(loc=4)
    ax.set_ylabel(f"Episode reward")
    ax.set_xlabel("episode #")
    ax.set_title(f"Rewards for every {SHOW_EVERY} episode")
    if SAVE_RESULTS:
        f.savefig(
            f"{OUT_FILE_DIR}/rewards_{N}x{M}_eps{EPS_DECAY}_lr{LEARNING_RATE}_g{GAMMA}_batch{BATCH_SIZE}_mem{MEMORY_SIZE}.png")
    else:
        plt.show()


plot_moving_average_reward(moving_avg)
plot_num_steps_per_episode(ep_durations)
plot_rewards(aggr_ep_rewards)
"""

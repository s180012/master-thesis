import numpy as np
import matplotlib.pyplot as plt
from environments.gridworld import GridWorld

import os
import cv2
import time


def makedir(directory):
    try:
        os.makedirs(directory)
    except OSError:
        print(f"Creation of the directory {directory} failed")
    else:
        print(f"Results stored in directory {directory}")


N, M = 7, 7
env = GridWorld(n=N, m=M, water=True)

start_time = time.strftime("%Y-%d-%m-%H:%M:%S", time.localtime(time.time()))
OUT_FILE_DIR = f"../runs/dqn/gridworld/moving_water/{start_time}"
makedir(f"{OUT_FILE_DIR}/states/")

all_water_positions = [(i,j) for i in range(N) for j in range(M)]
all_water_positions.pop()  # remove last location as that one is always occupied by the goal cell
# print(water_pos)
for water_pos in all_water_positions:
	for i in range(N):
		for j in range(M):
			if (i,j) != water_pos:
				env.reset(agent_pos=(i,j), water_pos=water_pos)
				norm_img_arr = env.render()  # get current state
				img = np.transpose(norm_img_arr, (1,2,0))

				cv2.imwrite(f"{OUT_FILE_DIR}/states/a{i}-{j}_w{water_pos[0]}-{water_pos[1]}.png", img*255)
				# plt.imshow(img)
				# plt.show()
				# time.sleep(2)
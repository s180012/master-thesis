import numpy as np
import matplotlib.pyplot as plt
from environments.gridworld import GridWorld

import cv2
import time


def max_action(q_table, state, actions):
    max_action_index = np.argmax([q_table[state, a] for a in actions])
    return actions[max_action_index]


N, M = 10, 10
env = GridWorld(n=N, m=M)
start_time = time.strftime("%Y-%d-%m-%H:%M:%S", time.localtime(time.time()))

LEARNING_RATE = 0.1
DISCOUNT = 0.9
EPISODES = 10000
SHOW_EVERY = 500
MOVING_AVERAGE_WINDOW = 500
EPS = 0.1
OUT_FILE_NAME = f"gridworld/eps/ma_eps{EPS}_{N}x{M}_{start_time}.png"

q_table = {}
for state in env.state_space:
    for action in env.action_space:
        q_table[state, action] = 0

ep_rewards = []
aggr_ep_rewards = {'ep': [], 'avg': [], 'min': [], 'max': []}

for episode in range(EPISODES):
    show = episode % SHOW_EVERY == 0  # True for every SHOW_EVERY'th episode

    observation = env.reset()

    ep_reward = 0

    done = False
    while not done:  # can be replaced by a for loop if we want to give up and move to next episode after some number of steps
        rand = np.random.random()
        if rand < (1 - EPS):
            action = max_action(q_table, observation, env.possible_actions)
        else:
            action = env.sample_action_space()

        new_observation, reward, done, _ = env.step(action)
        ep_reward += reward

        if not done:
            max_future_action = max_action(q_table, new_observation, env.possible_actions)
            max_future_q = q_table[new_observation, max_future_action]
            # update Q
            q_table[observation, action] = (1 - LEARNING_RATE) * q_table[observation, action] + LEARNING_RATE * (reward + DISCOUNT * max_future_q)
        elif new_observation == env.final_state:
            q_table[observation, action] = 0

        observation = new_observation

        # # display walk-through one episode
        # if show:
        #     img_arr = env.grid
        #     norm_img_arr = np.interp(img_arr, (img_arr.min(), img_arr.max()), (0, 1))  # normalizing the image to [0,1] range
        #     cv2.imshow("image", norm_img_arr)
        #     cv2.waitKey(500)

    # cv2.destroyAllWindows()  # destroys all created windows

    # print(f"Episode reward: {ep_reward}")
    ep_rewards.append(ep_reward)

    if show:
        avg_ep_reward = sum(ep_rewards[-SHOW_EVERY:]) / len(ep_rewards[-SHOW_EVERY:])
        min_ep_reward = min(ep_rewards[-SHOW_EVERY:])
        max_ep_reward = max(ep_rewards[-SHOW_EVERY:])

        aggr_ep_rewards['ep'].append(episode)
        aggr_ep_rewards['avg'].append(avg_ep_reward)
        aggr_ep_rewards['min'].append(min_ep_reward)
        aggr_ep_rewards['max'].append(max_ep_reward)
        print(f"ep {episode}; avg {avg_ep_reward}; min {min_ep_reward}; max {max_ep_reward}")
        # env.render()
        # time.sleep(1)
    # print('Done!')

moving_avg = np.convolve(ep_rewards, np.ones((MOVING_AVERAGE_WINDOW,)) / MOVING_AVERAGE_WINDOW, mode='valid')

plt.plot([i for i in range(len(moving_avg))], moving_avg)
plt.ylabel(f"Reward {MOVING_AVERAGE_WINDOW}ma")
plt.xlabel("episode #")
plt.title("Moving average for episode rewards")
plt.show()
# plt.savefig(f"runs/dqn/{OUT_FILE_NAME}")

plt.plot(aggr_ep_rewards['ep'], aggr_ep_rewards['avg'], label="avg rewards")
plt.plot(aggr_ep_rewards['ep'], aggr_ep_rewards['min'], label="min rewards")
plt.plot(aggr_ep_rewards['ep'], aggr_ep_rewards['max'], label="max rewards")
plt.legend(loc=4)
plt.show()
# plt.savefig(f"runs/dqn/{OUT_FILE_NAME}")


## Run VAE training on gridworld dataset
Runing training or evaluation on other gridworld dataset (smaller 841 images) has to be specified in the `run_VAE.py` file, line 97.

`python run_vae.py --epochs=200 --beta=1 --dataset=gridworld --log_interval=100 --latent_dim=10 --comment=full`

## Run VAE evaluation on gridworld datasets
Specify the dataset `--eval_set='<dataset>'` for which you want to get values of p(x) ~ ELBO.
The path to the model has to be specified manually in `run_VAE.py`, line 273.
The result is stored at the end of the log file `algorithms/eval_epoch_elbos.log`.

`python run_vae.py --epochs=200 --beta=1 --dataset=gridworld --log_interval=100 --latent_dim=4 --comment=full --eval --eval_set='new_danger'`

### Display elbos
Copy ELBO values from `algorithms/eval_epoch_elbos.log` to DataFrame:
```
df = pd.DataFrame({
    'test_set': pd.DataFrame({'elbo': [-5.847992, -5.9029975, -4.8087234, -10.079248, -6.1850147, -6.755953, -9.28668, -7.5024357, -5.58992, -7.6740313, -4.3585167, -7.3436966, -8.56885, -8.999521, -6.235569, -4.2838926, -6.018176, -6.9356017, -6.2350516, -5.500276, -5.0520267, -7.2276716]}).elbo,
    'not_in_training': pd.DataFrame({'elbo': [-8.009378]}).elbo,
    'new_danger': pd.DataFrame({'elbo': [-22.449228, -31.14753, -25.91006, -26.090538, -18.714516, -10.598148]}).elbo,
    'water_light': pd.DataFrame({'elbo': [-27.114374, -29.995853, -17.975061, -16.87668, -18.425177, -29.801113]}).elbo,
    'water_moved': pd.DataFrame({'elbo': [-52.43776, -72.18925, -39.414925, -40.22672, -39.143578, -33.647484]}).elbo,
    'water_color_change': pd.DataFrame({'elbo': [-33.211693, -40.46866, -35.796345, -34.167076, -34.45726, -35.387806]}).elbo
})

# plot ELBOs
fig, ax = plt.subplots(figsize=(10,5))
ax.grid()
ax.set_ylabel('p(x) ~ ELBO')
ax.set_xlabel('dataset')
ax.set_title('MLP VAE with 4 latent dimensions evaluated on gridworld datasets')
sns.stripplot(data=df)
```



## Generate gridworld dataset:
To generate images of all combinations of agent-water positions, run `python -m algorithms.generate_gridworld_dataset`. Results will be stored in `DTU/runs/dqn/gridworld/moving_water/<datetime>/states/`.

To split the data into train/dev/test split, run `python generate_train_dev_test_split.py` from folder `DTU/runs/dqn/gridworld/moving_water/<datetime>/`. Resulting csv files (train_set.csv, dev_set.csv, test_set.csv) are stored in `DTU/runs/dqn/gridworld/moving_water/<datetime>/`.

## Generate pong dataset:
Run `python -m algorithms.rl.pong_catastrophe_postprocessing`. Results are in `DTU/runs/dqn/pong/<datetime>/pixel-line-states-black-bg(-cropped)`.

import torch
import torch.nn as nn
import torch.nn.functional as F

class DQN(nn.Module):

	def __init__(self, in_features, n_actions=4):
		super(DQN, self).__init__()
		# self.conv1 = nn.Conv2d(in_channels, 6, kernel_size=1)
		# self.fc1 = nn.Linear(5 * 5 * 6, 128)
		self.fc1 = nn.Linear(in_features, 256)
		self.fc2 = nn.Linear(256, 256)
		self.fc3 = nn.Linear(256, 512)
		self.head = nn.Linear(512, n_actions)

	def forward(self, x):
		x = x.view(x.size(0), -1)
		x = F.relu(self.fc1(x))
		x = F.relu(self.fc2(x))
		x = F.relu(self.fc3(x))
		# x = F.relu(self.conv1(x))
		# x = x.view(x.size(0), -1)
		# x = F.relu(self.fc1(x))
		return self.head(x)


class DQNAtari(nn.Module):

	def __init__(self, h, w, n_actions=2):
		super(DQNAtari, self).__init__()
		self.conv1 = nn.Conv2d(1, 16, kernel_size=5, stride=2)
		self.bn1 = nn.BatchNorm2d(16)
		self.conv2 = nn.Conv2d(16, 32, kernel_size=5, stride=2)
		self.bn2 = nn.BatchNorm2d(32)
		self.conv3 = nn.Conv2d(32, 32, kernel_size=5, stride=2)
		self.bn3 = nn.BatchNorm2d(32)

		def conv2d_size_out(size, kernel_size=5, stride=2):
			return (size - (kernel_size - 1) - 1) // stride + 1

		out_conv_h = conv2d_size_out(conv2d_size_out(conv2d_size_out(h)))
		out_conv_w = conv2d_size_out(conv2d_size_out(conv2d_size_out(w)))
		linear_input_size = 32 * out_conv_h * out_conv_w

		self.head = nn.Linear(linear_input_size, n_actions)

	def forward(self, x):
		x = F.relu(self.bn1(self.conv1(x)))
		x = F.relu(self.bn2(self.conv2(x)))
		x = F.relu(self.bn3(self.conv3(x)))
		x = x.view(x.size(0), -1)
		return self.head(x)

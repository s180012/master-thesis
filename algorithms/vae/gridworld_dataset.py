import torch
import cv2
import os
import pandas as pd
from torch.utils.data import Dataset, DataLoader

class GridworldDataset(Dataset):
    """Gridworld dataset."""

    def __init__(self, csv_file, images_dir, transform=None):
        """
        Args:
            csv_file (string): Path to the csv file with annotations.
            images_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        self.gridworlds_df = pd.read_csv(csv_file, index_col=0)
        self.images_dir = images_dir
        self.transform = transform

    def __len__(self):
        return len(self.gridworlds_df)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        img_file = os.path.join(self.images_dir,
                                self.gridworlds_df.iloc[idx, 0])
        # image = cv2.imread(img_file, 0)  # 0 is used to load grayscale image
        image = cv2.imread(img_file)
#         image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

#         landmarks = self.landmarks_frame.iloc[idx, 1:]
#         landmarks = np.array([landmarks])
#         landmarks = landmarks.astype('float').reshape(-1, 2)
#         sample = {'image': image, 'landmarks': landmarks}

        if self.transform:
            image = self.transform(image)
            
        sample = {'image': image}
        label = torch.empty(1)
        return image, label

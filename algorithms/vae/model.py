import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.distributions import Normal, Bernoulli


class Encoder(nn.Module):
	def __init__(self, latent_dim):
		super(Encoder, self).__init__()

		# self.conv1 = nn.Conv2d(1, 16, 3)
		# self.conv2 = nn.Conv2d(16, 32, 4, 2)
		# self.conv3 = nn.Conv2d(32, 64, 4, 2)
		self.conv1 = nn.Conv2d(1, 16, 3)
		self.conv2 = nn.Conv2d(16, 32, (6, 4), 2)
		self.conv3 = nn.Conv2d(32, 64, 4, 2)
		self.fc1 = nn.Linear(64*20*18, 256)
		self.fc_mu = nn.Linear(256, latent_dim)
		self.fc_logvar = nn.Linear(256, latent_dim)

	def forward(self, x):
		batch_size = x.shape[0]
		# print(x.shape)
		x = F.relu(self.conv1(x))
		# print(x.shape)
		x = F.relu(self.conv2(x))
		# print(x.shape)
		x = F.relu(self.conv3(x))
		# print(x.shape)
		x = x.view(batch_size, -1)
		# print(x.shape)
		x = F.relu(self.fc1(x))
		# print(x.shape)
		x_mu = self.fc_mu(x)
		x_log_var = self.fc_logvar(x)

		return x_mu, x_log_var


class Decoder(nn.Module):
	def __init__(self, latent_dim):
		super(Decoder, self).__init__()

		self.fc1 = nn.Linear(latent_dim, 256)
		self.fc2 = nn.Linear(256, 64*20*18)
		self.conv1 = nn.ConvTranspose2d(64, 32, 4, 2)
		self.conv2 = nn.ConvTranspose2d(32, 16, (6, 4), 2)
		self.conv3 = nn.ConvTranspose2d(16, 1, 3)

	def forward(self, x):
		batch_size = x.shape[0]
		# print("decoder")
		# print(x.shape)
		x = F.relu(self.fc1(x))
		# print(x.shape)
		x = F.relu(self.fc2(x))
		# print(x.shape)
		x = x.view(batch_size, 64, 20, 18)
		# print(x.shape)
		x = F.relu(self.conv1(x))
		# print(x.shape)
		x = F.relu(self.conv2(x))
		# print(x.shape)
		x = self.conv3(x)
		# print(x.shape)
		x = torch.sigmoid(x)

		return x


class EncoderPong(nn.Module):
	def __init__(self, latent_dim):
		super(EncoderPong, self).__init__()

		self.conv1 = nn.Conv2d(1, 16, 3)
		self.bn1 = nn.BatchNorm2d(16)
		self.conv2 = nn.Conv2d(16, 32, (6, 4), 2)
		# self.conv2 = nn.Conv2d(16, 32, (4, 4), 2)
		self.bn2 = nn.BatchNorm2d(32)
		self.conv3 = nn.Conv2d(32, 64, 4, 2)
		self.bn3 = nn.BatchNorm2d(64)
		self.conv4 = nn.Conv2d(64, 64, 4, 2)
		self.bn4 = nn.BatchNorm2d(64)
		self.fc1 = nn.Linear(64*9*8, 256)
		# self.fc1 = nn.Linear(64*8*8, 256)
		self.bn5 = nn.BatchNorm1d(256)
		self.fc_mu = nn.Linear(256, latent_dim)
		self.fc_logvar = nn.Linear(256, latent_dim)

	def forward(self, x):
		batch_size = x.shape[0]
		# print(x.shape)
		x = F.relu(self.conv1(x))
		x = self.bn1(x)
		# print(x.shape)
		x = F.relu(self.conv2(x))
		x = self.bn2(x)
		# print(x.shape)
		x = F.relu(self.conv3(x))
		x = self.bn3(x)
		# print(x.shape)
		x = F.relu(self.conv4(x))
		x = self.bn4(x)
		# print(x.shape)
		x = x.view(batch_size, -1)
		# print(x.shape)
		x = F.relu(self.fc1(x))
		# print(x.shape)
		x = self.bn5(x)
		# print(x.shape)
		x_mu = self.fc_mu(x)
		x_log_var = self.fc_logvar(x)

		return x_mu, x_log_var


class DecoderPong(nn.Module):
	def __init__(self, latent_dim):
		super(DecoderPong, self).__init__()

		self.fc1 = nn.Linear(latent_dim, 256)
		self.bn1 = nn.BatchNorm1d(256)
		self.fc2 = nn.Linear(256, 64*9*8)
		self.bn2 = nn.BatchNorm1d(64*9*8)
		# self.fc2 = nn.Linear(256, 64*8*8)
		self.conv1 = nn.ConvTranspose2d(64, 64, 4, 2)
		self.bn3 = nn.BatchNorm2d(64)
		self.conv2 = nn.ConvTranspose2d(64, 32, 4, 2)
		self.bn4 = nn.BatchNorm2d(32)
		self.conv3 = nn.ConvTranspose2d(32, 16, (6, 4), 2)
		self.bn5 = nn.BatchNorm2d(16)
		# self.conv3 = nn.ConvTranspose2d(32, 16, (4, 4), 2)
		self.conv4 = nn.ConvTranspose2d(16, 1, 3)

	def forward(self, x):
		batch_size = x.shape[0]
		# print("decoder")
		# print(x.shape)
		x = F.relu(self.fc1(x))
		x = self.bn1(x)
		# print(x.shape)
		x = F.relu(self.fc2(x))
		x = self.bn2(x)
		# print(x.shape)
		x = x.view(batch_size, 64, 9, 8)
		# x = x.view(batch_size, 64, 8, 8)
		# print(x.shape)
		x = F.relu(self.conv1(x))
		x = self.bn3(x)
		# print(x.shape)
		x = F.relu(self.conv2(x))
		x = self.bn4(x)
		# print(x.shape)
		x = F.relu(self.conv3(x))
		x = self.bn5(x)
		# print(x.shape)
		x = self.conv4(x)
		# print(x.shape)
		x = torch.sigmoid(x)

		return x


class MLPEncoder(nn.Module):
	def __init__(self, latent_dim, img_size):
		super(MLPEncoder, self).__init__()

		self.fc1 = nn.Linear(3*img_size[0]*img_size[1], 512)
		self.fc2 = nn.Linear(512, 256)
		self.fc_mu = nn.Linear(256, latent_dim)
		self.fc_logvar = nn.Linear(256, latent_dim)

	def forward(self, x):
		batch_size = x.shape[0]
		x = x.view(batch_size, -1)
		x = F.relu(self.fc1(x))
		x = F.relu(self.fc2(x))
		x_mu = self.fc_mu(x)
		x_log_var = self.fc_logvar(x)

		return x_mu, x_log_var


class MLPDecoder(nn.Module):
	def __init__(self, latent_dim, img_size):
		super(MLPDecoder, self).__init__()

		self.img_size = img_size
		self.fc1 = nn.Linear(latent_dim, 256)
		self.fc2 = nn.Linear(256, 512)
		self.fc3 = nn.Linear(512, 3*img_size[0]*img_size[1])

	def forward(self, x):
		batch_size = x.shape[0]
		x = F.relu(self.fc1(x))
		x = F.relu(self.fc2(x))
		x = self.fc3(x)
		x = x.view(batch_size, 3, self.img_size[0], self.img_size[1])
		x = torch.sigmoid(x)

		return x


class MLPEncoderOne(nn.Module):
	def __init__(self, latent_dim, img_size):
		super(MLPEncoderOne, self).__init__()

		self.fc1 = nn.Linear(3*img_size[0]*img_size[1], 64)
		self.bn1 = nn.BatchNorm1d(64)
		self.dropout = nn.Dropout(p=0.2)
		self.fc_mu = nn.Linear(64, latent_dim)
		self.fc_logvar = nn.Linear(64, latent_dim)

		torch.nn.init.xavier_uniform_(self.fc1.weight)
		torch.nn.init.xavier_uniform_(self.fc_mu.weight)
		torch.nn.init.xavier_uniform_(self.fc_logvar.weight)

	def forward(self, x):
		batch_size = x.shape[0]
		x = x.view(batch_size, -1)
		x = self.dropout(self.bn1(F.relu(self.fc1(x))))
		x_mu = self.fc_mu(x)
		x_log_var = self.fc_logvar(x)

		return x_mu, x_log_var


class MLPDecoderOne(nn.Module):
	def __init__(self, latent_dim, img_size):
		super(MLPDecoderOne, self).__init__()

		self.img_size = img_size
		self.fc1 = nn.Linear(latent_dim, 64)
		self.fc_out = nn.Linear(64, 3*img_size[0]*img_size[1])

		torch.nn.init.xavier_uniform_(self.fc1.weight)
		torch.nn.init.xavier_uniform_(self.fc_out.weight)

	def forward(self, x):
		batch_size = x.shape[0]
		x = F.relu(self.fc1(x))
		x = self.fc_out(x)
		x = x.view(batch_size, 3, self.img_size[0], self.img_size[1])
		x = torch.sigmoid(x)

		return x


class VAE(nn.Module):
	def __init__(self, encoder, decoder):
		super(VAE, self).__init__()

		self.encoder = encoder
		self.decoder = decoder

	def reparameterize(self, x_mu, x_std):
		q_zx = Normal(x_mu, x_std)
		p_z = Normal(torch.zeros_like(x_mu), torch.ones_like(x_std))
		return q_zx, p_z

	def forward(self, x):
		out = dict()

		x_mu, x_logvar = self.encoder(x)
		x_std = torch.exp(x_logvar / 2)  # sigma = e^(log_var / 2)

		# q(z|x) = Normal(z | x_mu, x_logvar)
		q_zx = Normal(x_mu, x_std)
		p_z = Normal(torch.zeros_like(x_mu), torch.ones_like(x_std))
#         q_zx, p_z = self.reparameterize(x_mu, x_std)

		# sample from q(z|x) with reparametrization
		z = q_zx.rsample()

		# p(x|z) = Bernoulli()
		z_mu = self.decoder(z)
		z_mu = z_mu.view(z_mu.shape[0], -1)
		p_xz = Bernoulli(z_mu)

		out['q_zx'] = q_zx
		out['p_z'] = p_z
		out['z'] = z
		out['z_mu'] = z_mu
		out['p_xz'] = p_xz
		return out

import torch
import cv2
import os
import pandas as pd
from torch.utils.data import Dataset, DataLoader

LABELS = {
    'none': 0,
    'bottom': 1,
    'top': 2
}

class PongDataset(Dataset):
    """Pong dataset."""

    def __init__(self, csv_file, images_dir, transform=None, classes=None):
        """
        Args:
            csv_file (string): Path to the csv file with annotations.
            images_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a sample. (default: None)
            classes (list): List of catastrophe classes to use. (default: None)
        """
        self.pong_df = pd.read_csv(csv_file, index_col=0)
        if classes:
            self.pong_df = self.pong_df.loc[self.pong_df.catastrophe.isin(classes)]
        self.images_dir = images_dir
        self.transform = transform

    def __len__(self):
        return len(self.pong_df)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        img_file = os.path.join(self.images_dir,
                                self.pong_df.iloc[idx, 0])
        # image = cv2.imread(img_file, 0)  # 0 is used to load grayscale image
        image = cv2.imread(img_file, 0)

        if self.transform:
            image = self.transform(image)
            
        sample = {'image': image}
        action = self.pong_df.iloc[idx, 1]
        # label = torch.empty(1)
        label = self.pong_df.iloc[idx, 2]
        return image, label

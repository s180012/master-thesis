import numpy as np
import itertools
import torch
import cv2
from PIL import Image
# import matplotlib.pyplot as plt

ENV_OBJECTS = {
	'bg': 0,
	'agent': 1,
	'destination': 2,
	'water': 3
}

COLORS = {
	'bg': [0.0, 0.0, 0.0],
	'agent': [1.0, 1.0, 1.0],
	'destination': [0.5, 0.5, 0.5],
	'water': [1.0, 0.0, 0.0]
}

class GridWorld:
	"""docstring for GridWorld"""
	def __init__(self, n, m, water=False):
		self.include_water = water
		self.n = n  # n rows
		self.m = m  # m columns
		self.grid = np.zeros((n, m))

		self.state_space = list(itertools.product(range(self.m), range(self.n)))  # used for Q-learning
		# self.final_state = (m - 1, n - 1)
		self.goal = CustomObject(m - 1, n - 1, ENV_OBJECTS['destination'])
		# self.final_state_grid = np.zeros((n, m))
		# self.final_state_grid[self.final_state] = ENV_OBJECTS['agent']
		self.agent = CustomObject(0, 0, ENV_OBJECTS['agent'])
		# self.agent_position = {'x': 0, 'y': 0}  # acts as the current state

		# self.is_final_state = False

		self.action_space = {0: (0, -1), 1: (0, 1), 2: (-1, 0), 3: (1, 0)}
		self.action_space_mapping = {0: 'up', 1: 'down', 2: 'left', 3: 'right'}
		self.possible_actions = [0, 1, 2, 3]

		self.grid[self.agent.get_position()] = self.agent.obj_type
		# self.grid[self.agent_position['y'], self.agent_position['x']] = ENV_OBJECTS['agent']
		self.grid[self.goal.get_position()] = self.goal.obj_type
		# self.grid[self.final_state] = ENV_OBJECTS['destination']

		# store available locations for random water blocks
		self.available_locations = list(itertools.product(range(self.m), range(self.n)))
		self.available_locations.remove(self.agent.get_position())
		self.available_locations.remove(self.goal.get_position())
		if self.include_water:
			self.water_object = CustomObject(1, 1, ENV_OBJECTS['water'])
			self.grid[self.water_object.get_position()] = self.water_object.obj_type

	def step(self, action):

		# move_x, move_y = self.action_space[action]
		# self.agent.move(move_x, move_y)
		new_state, is_catastrophe = self._perform_action(action)
		# self._set_state(new_state)

		# self.is_final_state = np.array_equal(new_state, self.final_state_grid)
		is_final_state = self.agent.get_position() == self.goal.get_position()

		reward = self._get_reward(is_catastrophe, is_final_state)
		done = is_catastrophe or is_final_state

		return self.get_state(), reward, done, None  # return observation, reward, done, info

	def reset(self, random_water=False, agent_pos=None, water_pos=None):
		self.grid = np.zeros_like(self.grid, dtype=np.uint8)
		self.grid[self.goal.get_position()] = self.goal.obj_type
		# self.grid[self.final_state] = ENV_OBJECTS['destination']

		# self.agent_position = {'x': 0, 'y': 0}
		if agent_pos:
			self.agent.set_position(*agent_pos)
		else:
			self.agent.set_position(0, 0)
		self.grid[self.agent.get_position()] = self.agent.obj_type
		# self.grid[self.agent_position['y'], self.agent_position['x']] = ENV_OBJECTS['agent']

		# self.final_state_grid = np.zeros((self.n, self.m))
		# self.final_state_grid[self.final_state] = ENV_OBJECTS['agent']
		if self.include_water:
			if random_water:
				water_location = self.available_locations[np.random.choice(range(len(self.available_locations)))]
				self.water_object.set_position(water_location[0], water_location[1])
			elif water_pos:
				self.water_object.set_position(*water_pos)
			self.grid[self.water_object.get_position()] = self.water_object.obj_type

		# self.is_final_state = False

		# return (self.agent_position['x'], self.agent_position['y'])
		return self.get_state()

	def render(self):
		img = np.zeros((3, self.n, self.m))

		# following code might be helpful for RGB images
		for i in range(self.n):
			for j in range(self.m):
				if self.grid[i][j] == 0:
					img[0, i, j] = COLORS['bg'][0]
					img[1, i, j] = COLORS['bg'][1]
					img[2, i, j] = COLORS['bg'][2]
				elif self.grid[i][j] == ENV_OBJECTS['agent']:
					img[0, i, j] = COLORS['agent'][0]
					img[1, i, j] = COLORS['agent'][1]
					img[2, i, j] = COLORS['agent'][2]
				elif self.grid[i][j] == ENV_OBJECTS['destination']:
					img[0, i, j] = COLORS['destination'][0]
					img[1, i, j] = COLORS['destination'][1]
					img[2, i, j] = COLORS['destination'][2]
				elif self.grid[i][j] == ENV_OBJECTS['water']:
					img[0, i, j] = COLORS['water'][0]
					img[1, i, j] = COLORS['water'][1]
					img[2, i, j] = COLORS['water'][2]

		# scale up the environment array to have appropriate size for displaying
		# max_dim = max(self.n, self.m)
		# scaling_factor = 300 // max_dim
		# img = np.kron(img, np.ones((scaling_factor, scaling_factor)))

		# img = Image.fromarray(np.uint8(env), 'L')  # 'L' is used for grayscale image
		# img = img.resize((300, 300))
		# cv2.imshow('image', np.array(img))
		# img.show()
		norm_img = np.interp(img, (img.min(), img.max()), (0, 1))  # normalizing the image to [0,1] range
		return norm_img

	def sample_action_space(self):
		# return np.random.choice(np.array(self.possible_actions))
		return np.random.randint(0, len(self.possible_actions))

	def get_state(self):
		state = np.copy(self.grid)
		# get state in shape (CWH)
		state = np.expand_dims(state, axis=0)
		# get state in shape (BCWH)
		state = np.expand_dims(state, axis=0)
		return torch.from_numpy(state).float()

	@staticmethod
	def _get_reward(is_catastrophe, is_final_state):
		if is_catastrophe:
			return -50
		elif is_final_state:
			return 0
		else:
			return -1

	def _perform_action(self, action):
		"""returns the resulting state after applying the action"""
		is_catastrophe = False
		move_x, move_y = self.action_space[action]
		# agent_x = self.agent_position['x']
		# agent_y = self.agent_position['y']
		agent_x, agent_y = self.agent.get_position()

		self.grid[self.agent.get_position()] = ENV_OBJECTS['bg']

		new_agent_x = agent_x + move_x
		new_agent_y = agent_y + move_y
		# handle movements out of grid
		if 0 <= new_agent_x < self.m:
			agent_x = new_agent_x

		if 0 <= new_agent_y < self.n:
			agent_y = new_agent_y

		# set new agent position
		self.agent.set_position(agent_x, agent_y)

		# handle collision with water
		if self.agent.get_position() == self.water_object.get_position():
			is_catastrophe = True
		else:
			self.grid[self.agent.get_position()] = self.agent.obj_type
		# self.agent_position['x'] = agent_x
		# self.agent_position['y'] = agent_y
		# self.grid[self.agent_position['y'], self.agent_position['x']] = ENV_OBJECTS['agent']

		# return (agent_x, agent_y)
		return self.grid, is_catastrophe


class CustomObject:
	"""docstring for CustomObject"""
	def __init__(self, x, y, obj_type):
		self.x = x
		self.y = y
		# self.env_size_x, self.env_size_y = env_size
		self.obj_type = obj_type

	def set_position(self, x, y):
		self.x = x
		self.y = y

	def get_position(self):
		return self.x, self.y

	# def move(self, x, y):
	# 	self.x += x
	# 	self.y += y
	#
	# 	# handle agent moving out of the world's boundries
	# 	if self.x < 0:
	# 		self.x = 0
	# 	elif self.x > self.env_size_x - 1:
	# 		self.x = self.env_size_x - 1
	#
	# 	if self.y < 0:
	# 		self.y = 0
	# 	elif self.y > self.env_size_y - 1:
	# 		self.y = self.env_size_y - 1


# env = GridWorld(2, 4)
# print(env.grid)
